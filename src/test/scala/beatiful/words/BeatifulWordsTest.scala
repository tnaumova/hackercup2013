package beatiful.words

import org.scalatest.FlatSpec
import facebook.zero.beatiful.words.WordsAnalyzer
import scalax.io.Output
import scala.io.Source
import scalax.io.Resource
import java.io.File

class BeatifulWordsTest extends FlatSpec {
 lazy val analyzer = new WordsAnalyzer("/Users/tanya/Downloads/input.txt")
 
  "1" should "parse file" in {
    assert(List("ABbCcc",
"Good luck in the Facebook Hacker Cup this year!",
"Ignore punctuation, please :)",
"Sometimes test cases are hard to make up.",
"So I just go consult Professor Dalves") === analyzer.parseInput);
    
    
//    Case #1: 152
//Case #2: 754
//Case #3: 491
//Case #4: 729
//Case #5: 646
  }
 
  it should "Split string to words" in {
    assert(Array("ignore", "punctuation", "please") === analyzer.splitToChars("Ignore punctuation, please :)"))
  } 
  
  "2" should "map to number of occurences ordered" in {
    assert(List(("c", 3), ("b", 2), ("a", 1)).toString === analyzer.combineAll("ABbCcc").toString )
  }
  
  "3" should "assign maximized values to characters" in {
    assert(List(("c", 26*3), ("b", 25*2), ("a", 24), ("d", 23)).sorted.toString === analyzer.assignValues("abbccc d").sorted.toString)
  }
  
  "4" should "calculate word score" in {
    assert(152 === analyzer.calculate("abbccc"))
    }
  
  "5" should "calculate beauty of string" in {
    assert(152 === analyzer.calculateStringBeauty("ABbCcc"))
    assert(754 === analyzer.calculateStringBeauty("Good luck in the Facebook Hacker Cup this year!"))
    assert(491 === analyzer.calculateStringBeauty("Ignore punctuation, please :)"))
    assert(729 === analyzer.calculateStringBeauty("Sometimes test cases are hard to make up."))
    assert(646 === analyzer.calculateStringBeauty("So I just go consult Professor Dalves"))
  }
 
}
