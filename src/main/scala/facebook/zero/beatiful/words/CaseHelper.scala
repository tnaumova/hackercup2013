package facebook.zero.beatiful.words

import scala.io._
import scala.io
import scalax.io._
import java.io.File

trait CaseHelper {

  def readCases(p: String): Seq[String] = {
   Source.fromURL(
     getClass.getResource(p), "UTF-8"
   ).getLines.toSeq.tail
 }

 def writeCases(namespace: String ,cc: Seq[String]): File = {
   val tempFile: File = File.createTempFile(namespace, namespace)
   val output:Output = Resource.fromFile(tempFile)

   output.writeStrings(appendNumbers(1, cc))

   def appendNumbers(n: Int, cc: Seq[String]): Seq[String] = {
     cc.toList match {
       case Nil => cc
       case x :: xs => (("Case #" + n.toString + ": " + x + "\n") :: appendNumbers(n + 1, xs).toList).toSeq
     }
   }

   return tempFile
 }
}