package facebook.zero.beatiful.words

import scala.io.Source;
import java.io.File;
import scalax.io._;

class WordsAnalyzer(path:String) extends CaseHelper {

  
  def parseInput : List[String]= {
    Source.fromFile(path).getLines.toList.tail
  }
  
  def splitToChars(str: String) : Array[String] = {
    str.toLowerCase.replaceAll("[^\\w\\s]", "").split(" ")
  }
  
  def combine(str: String): List[(Char,Int)] = {
    str.toCharArray.foldLeft(Map.empty[Char,Int])((map: Map[Char, Int],ch: Char) => {
      map.updated(ch, map.getOrElse(ch, 0) + 1)
    }).toList
  }
  
  
  def assignValues(str: String): List[(Char,Int)] = {
    //println("Combine "+combineAll(str))
    combineAll(str).foldLeft((List.empty[(Char,Int)], 26))((res: (List[(Char,Int)], Int), el: (Char,Int)) => {
      ((el._1, el._2 * res._2) :: res._1 , res._2-1)} )._1
  }
  
  def calculate(str: String) : Int = { 
    assignValues(str).map(_._2).foldLeft(0)(_ + _)
  }
  
  def combineAll(str: String): List[(Char, Int)] = {
    splitToChars(str).map(el => combine(el)).flatten.foldLeft(Map.empty[Char,Int])((res, el) => {
      res.updated(el._1, res.getOrElse(el._1, 0) + el._2)
    }).toList.sortWith(_._2 >_._2)
  }
  
  
  def calculateStringBeauty(str: String): Int = {
    //println(assignValues(str))
   assignValues(str).foldLeft(0)(_ + _._2)
  }
  
  
  def execute = {
    //println( parseInput.map(row => String valueOf calculateStringBeauty(row)))
    println(writeCases("output.txt", 
        parseInput.map(row => String valueOf calculateStringBeauty(row))
    ).toString())
  }
}