scalaVersion := "2.10.0"

name := "Beautiful words"

version := "0.1"

mainClass = RunAnalyzer("foo.Foo")

libraryDependencies += "org.scalatest" %% "scalatest" % "1.9.1" % "test"

libraryDependencies += "com.github.scala-incubator.io" %% "scala-io-core" % "0.4.2"

libraryDependencies += "com.github.scala-incubator.io" %% "scala-io-file" % "0.4.2"